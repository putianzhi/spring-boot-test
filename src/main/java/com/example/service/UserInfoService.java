package com.example.service;

import com.example.entity.UserInfoEntity;

import java.util.List;

/**
 * @author wales
 * <p>
 * Service interface
 */
public interface UserInfoService {

    /**
     * find all method
     *
     * @return {@link UserInfoEntity}
     */
    List<UserInfoEntity> findAll();
}
