package com.example.service;

import com.example.dao.UserInfoMapper;
import com.example.entity.UserInfoEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * 业务代码
 *
 * @author wales
 */
@Service
@Transactional
@Slf4j
public class UserInfoServiceImpl implements UserInfoService {

    @Resource
    private UserInfoMapper userInfoMapper;

    /**
     * find all method
     *
     * @return {@link UserInfoEntity}
     */
    @Override
    public List<UserInfoEntity> findAll() {
        return userInfoMapper.findAllUserInfo();
    }
}
