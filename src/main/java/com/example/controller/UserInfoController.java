package com.example.controller;

import com.example.service.UserInfoService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author wales
 */
@RestController
@RequestMapping(value = "/")
public class UserInfoController {

    @Resource
    private UserInfoService userInfoService;

    @PostMapping(value = "/test")
    public Object find() {
        return userInfoService.findAll();
    }

}
