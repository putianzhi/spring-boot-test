package com.example.dao;

import com.example.entity.UserInfoEntity;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

import java.util.List;
import java.util.UUID;

@SpringBootTest
@Slf4j
class UserInfoMapperTest {

    @Resource
    private UserInfoMapper userInfoMapper;

    @Test
    void test1() {
        // query db
        List<UserInfoEntity> list = userInfoMapper.findAllUserInfo();

        // list [].get(0)
        String name = list.get(0).getName();

        // name -> method
        UserInfoEntity entity = userInfoMapper.findUserName(name);

        // print entity
        log.info("\n---> find user name : {}", entity);
    }

    /**
     * 查询单个用户
     */
    @Test
    void findOnlyUserInfo() {
        log.info("\n---> find only user info: {}", userInfoMapper.findOnlyUserInfo("1"));
    }

    /**
     * 查询所有用户
     */
    @Test
    void findAllUserInfo() {
        log.info("\n---> find all user info: {}", userInfoMapper.findAllUserInfo());
    }

    /**
     * 插入用户信息
     */
    @Test
    void insertUserInfo() {
        UserInfoEntity userInfoEntity = new UserInfoEntity();
        userInfoEntity.setId(UUID.randomUUID().toString());
        userInfoEntity.setName("姓名88");
        userInfoEntity.setAge(241112);
        userInfoEntity.setAddress(1612312306);
        int status = userInfoMapper.insertUserInfo(userInfoEntity);
        if (status == 1) {
            log.info("---> insert user info was successfully: {}", status);
        } else {
            log.info("---> insert user info was error: {}", status);
        }
    }

    /**
     * 更新用户信息
     */
    @Test
    void updateUserInfo() {
        UserInfoEntity userInfoEntity = new UserInfoEntity();
        // ID保持不变
        userInfoEntity.setId("1234");
        userInfoEntity.setName("姓名123");
        userInfoEntity.setAge(24);
        userInfoEntity.setAddress(1606);
        int status = userInfoMapper.updateUserInfo(userInfoEntity);
        if (status == 1) {
            log.info("---> update user info was successfully: {}", status);
        } else {
            log.info("---> update user info was error: {}", status);
        }
    }

    /**
     * 根据全局唯一ID来删除用户信息
     */
    @Test
    void deleteUserInfo() {
        UserInfoEntity name = userInfoMapper.findUserName("姓名88");
        String id = name.getId();
        int i = userInfoMapper.deleteUserInfo(id);
        if (i == 1) {
            log.info("---> delete user info was successfully: {}", i);
        } else {
            log.info("---> delete user info was error: {}", i);
        }
    }
}
