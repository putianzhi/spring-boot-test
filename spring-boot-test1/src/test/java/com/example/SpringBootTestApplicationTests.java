package com.example;

import com.example.entity.UserInfoEntity;
import com.example.service.UserInfoService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class SpringBootTestApplicationTests {
    @Autowired
    UserInfoService userInfoService;

    @Test
    void a() {
        System.out.println(userInfoService.findAllUserInfo());
    }

}
