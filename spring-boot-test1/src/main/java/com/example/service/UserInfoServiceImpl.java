package com.example.service;

import com.example.dao.UserInfoMapper;
import com.example.entity.UserInfoEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * 业务代码
 *
 * @author wales
 */
@Service

@Slf4j
public class UserInfoServiceImpl implements UserInfoService {
    @Resource
    UserInfoMapper userInfoMapper;

    @Override
    public UserInfoEntity findUserName(String name123) {
        return userInfoMapper.findUserName(name123);
    }

    @Override
    public UserInfoEntity findOnlyUserInfo(String id) {
        return userInfoMapper.findOnlyUserInfo(id);
    }

    @Override
    public List<UserInfoEntity> findAllUserInfo() {
        return userInfoMapper.findAllUserInfo();
    }

    @Override
    public int insertUserInfo(UserInfoEntity userInfoEntity) {
        return userInfoMapper.insertUserInfo(userInfoEntity);
    }

    @Override
    public int updateUserInfo(UserInfoEntity userInfoEntity) {
        return userInfoMapper.updateUserInfo(userInfoEntity);
    }

    @Override
    public int deleteUserInfo(String id) {
        return userInfoMapper.deleteUserInfo(id);
    }
}
