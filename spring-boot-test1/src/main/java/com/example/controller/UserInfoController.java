package com.example.controller;

import com.example.entity.UserInfoEntity;
import com.example.service.UserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author wales
 */
@RestController
@RequestMapping(value = "/")
public class UserInfoController {

    @Resource
    private UserInfoService userInfoService;



    @PostMapping(value = "/test")
    public Object find() {
        return userInfoService.findAllUserInfo();
    }


    @GetMapping("/select/{id}")
    public  Object selectById(@PathVariable String id){
        return userInfoService.findOnlyUserInfo(id);
    }
    @GetMapping("/select1/{name}")
        public Object selectByName(@PathVariable String name){
        return userInfoService.findUserName(name);
    }

    @GetMapping("/delete/{id}")
        public Object deleteById(@PathVariable String id){
        return userInfoService.deleteUserInfo(id);
    }

    @PostMapping("/insert")
    public Object insert(@RequestBody UserInfoEntity userInfoEntity){

        return userInfoService.insertUserInfo(userInfoEntity);
    }

    @PutMapping("/update")
    public Object update(@RequestBody  UserInfoEntity userInfoEntity){

        return userInfoService.updateUserInfo(userInfoEntity);
    }


}
